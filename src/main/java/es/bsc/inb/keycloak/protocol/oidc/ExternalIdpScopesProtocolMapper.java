/*
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.bsc.inb.keycloak.protocol.oidc;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import static org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider.OAUTH2_GRANT_TYPE_REFRESH_TOKEN;
import static org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider.OAUTH2_PARAMETER_CLIENT_ID;
import static org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider.OAUTH2_PARAMETER_CLIENT_SECRET;
import static org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider.OAUTH2_PARAMETER_GRANT_TYPE;
import static org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider.OAUTH2_PARAMETER_SCOPE;
import org.keycloak.broker.oidc.OIDCIdentityProvider;
import org.keycloak.broker.provider.IdentityProvider;
import static org.keycloak.broker.provider.IdentityProvider.FEDERATED_ACCESS_TOKEN;
import org.keycloak.broker.provider.util.SimpleHttp;
import org.keycloak.events.Details;
import org.keycloak.jose.jws.JWSInput;
import org.keycloak.jose.jws.JWSInputException;
import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.FederatedIdentityModel;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperContainerModel;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.ProtocolMapperConfigException;
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper;
import org.keycloak.protocol.oidc.mappers.UserInfoTokenMapper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.JsonWebToken;
import org.keycloak.representations.UserInfo;
import org.keycloak.services.resources.IdentityBrokerService;
import org.keycloak.util.JsonSerialization;
import org.keycloak.vault.VaultStringSecret;

/**
 * @author Dmitry Repchevsky
 */

public class ExternalIdpScopesProtocolMapper extends AbstractOIDCProtocolMapper 
        implements OIDCAccessTokenMapper, UserInfoTokenMapper {

    public static final String PROVIDER_ID = "external-idp-claims-mapper";
    public static final String PROVIDER_DISPLAY_TYPE = "External IDP Claim Mapper";
    public static final String PROVIDER_HELP_TEXT = "Maps an external IDP claim into the token";
    
    public final static String MAPPER_EXT_IDP_PROPERTY_NAME = "mapper-external-idp";
    public final static String MAPPER_EXT_IDP_PROPERTY_LABEL = "External IDP";
    public final static String MAPPER_EXT_IDP_PROPERTY_TITLE = "The external IDP";

    public final static String EXTERNAL_IDP_CLAIM_PROPERTY_NAME = "external-idp-claim-name";
    public final static String EXTERNAL_IDP_CLAIM_PROPERTY_LABEL = "External claim name";
    public final static String EXTERNAL_IDP_CLAIM_PROPERTY_TITLE = "External IDP claim name";

    public final static String TOKEN_CLAIM_PROPERTY_NAME = "token-claim-name";
    public final static String TOKEN_CLAIM_PROPERTY_LABEL = "Token claim name";
    public final static String TOKEN_CLAIM_PROPERTY_TITLE = "Token claim name";
    
    private final static List<ProviderConfigProperty> config_properties;

    static {
        final ProviderConfigProperty ext_idp = 
                new ProviderConfigProperty(
                        MAPPER_EXT_IDP_PROPERTY_NAME, 
                        MAPPER_EXT_IDP_PROPERTY_LABEL, 
                        MAPPER_EXT_IDP_PROPERTY_TITLE,
                        ProviderConfigProperty.STRING_TYPE, 
                        null);

        final ProviderConfigProperty ext_idp_claim =
                new ProviderConfigProperty(
                        EXTERNAL_IDP_CLAIM_PROPERTY_NAME, 
                        EXTERNAL_IDP_CLAIM_PROPERTY_LABEL, 
                        EXTERNAL_IDP_CLAIM_PROPERTY_TITLE,
                        ProviderConfigProperty.STRING_TYPE,
                        null);

        final ProviderConfigProperty token_claim =
                new ProviderConfigProperty(
                        TOKEN_CLAIM_PROPERTY_NAME, 
                        TOKEN_CLAIM_PROPERTY_LABEL, 
                        TOKEN_CLAIM_PROPERTY_TITLE,
                        ProviderConfigProperty.STRING_TYPE,
                        null);

        config_properties = List.of(ext_idp, ext_idp_claim, token_claim);
    }

    @Override
    public String getDisplayType() {
        return PROVIDER_DISPLAY_TYPE;
    }

    @Override
    public String getDisplayCategory() {
        return TOKEN_MAPPER_CATEGORY;
    }

    @Override
    public String getHelpText() {
        return PROVIDER_HELP_TEXT;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return config_properties;

    }

    @Override
    public int getPriority() {
        return 1000;
    }
 
    @Override
    public void validateConfig(KeycloakSession ksession, RealmModel realm, 
            ProtocolMapperContainerModel client, ProtocolMapperModel model) 
            throws ProtocolMapperConfigException {
        final String ext_idp_alias = model.getConfig().get(MAPPER_EXT_IDP_PROPERTY_NAME);
        if (ext_idp_alias == null || ext_idp_alias.isEmpty()) {
            throw new ProtocolMapperConfigException("empty " + MAPPER_EXT_IDP_PROPERTY_LABEL);
        } else {
            try (Stream<IdentityProviderModel> providers = realm.getIdentityProvidersStream()) {
                if (providers.filter(p -> ext_idp_alias.equals(p.getAlias())).count() == 0) {
                    throw new ProtocolMapperConfigException("invalid external idp: " + ext_idp_alias, "{0}");                
                }
            }
        }
        
        final String ext_idp_claim = model.getConfig().get(EXTERNAL_IDP_CLAIM_PROPERTY_NAME);
        if (ext_idp_claim == null || ext_idp_claim.isEmpty()) {
            throw new ProtocolMapperConfigException("empty " + EXTERNAL_IDP_CLAIM_PROPERTY_LABEL);
        }
        
        final String local_idp_claim = model.getConfig().get(TOKEN_CLAIM_PROPERTY_NAME);
        if (local_idp_claim == null || local_idp_claim.isEmpty()) {
            throw new ProtocolMapperConfigException("empty " + TOKEN_CLAIM_PROPERTY_LABEL);         
        }

    }
    
    @Override
    public AccessToken transformAccessToken(AccessToken token, ProtocolMapperModel model, 
            KeycloakSession ksession, UserSessionModel usession, ClientSessionContext ctx) {
        final String ext_token = getExternalAccessToken(model, ksession, usession);
        if (ext_token != null) {
            try {
                final JWSInput jws = new JWSInput(ext_token);
                final JsonWebToken jwt = jws.readJsonContent(JsonWebToken.class);
                mergeClaim(token, model, jwt.getOtherClaims());
            } catch (JWSInputException ex) {
                Logger.getLogger(ExternalIdpScopesProtocolMapper.class.getName()).log(Level.INFO, ex.getMessage());
            }
        }
        return token;
    }
    
    @Override
    public AccessToken transformUserInfoToken(AccessToken token, ProtocolMapperModel model, 
            KeycloakSession ksession, UserSessionModel usession, ClientSessionContext ctx) {
        final String ext_idp_alias = model.getConfig().get(MAPPER_EXT_IDP_PROPERTY_NAME);
        
        if (ext_idp_alias != null) {
            final OIDCIdentityProvider provider = (OIDCIdentityProvider)IdentityBrokerService.getIdentityProvider(ksession, usession.getRealm(), ext_idp_alias);       
            final String endpoint = provider.getConfig().getUserInfoUrl();
            if (endpoint != null) {
                final String ext_token = getExternalAccessToken(model, ksession, usession);
                if (ext_token != null) {
                    final UserInfo user_info = getExternalUserInfo(endpoint, ext_token);
                    final Map<String, Object> claims = user_info.getOtherClaims();
                    mergeClaim(token, model, claims);
                }
            }
        }
        return token;
    }

    private String getExternalAccessToken(ProtocolMapperModel model, 
            KeycloakSession ksession, UserSessionModel usession) {

        final String ext_idp_alias = model.getConfig().get(MAPPER_EXT_IDP_PROPERTY_NAME);
        final String ext_idp_claim = model.getConfig().get(EXTERNAL_IDP_CLAIM_PROPERTY_NAME);
        if (ext_idp_alias != null && ext_idp_claim != null) {
            // exchange token to the external one
            OIDCIdentityProvider provider = (OIDCIdentityProvider)IdentityBrokerService
                    .getIdentityProvider(ksession, usession.getRealm(), ext_idp_alias);
            if (provider.getConfig().isStoreToken()) {
                final FederatedIdentityModel fimodel = ksession.users().getFederatedIdentity(
                        usession.getRealm(), usession.getUser(), provider.getConfig().getAlias());
                if (fimodel != null) {
                    String ext_token = fimodel.getToken();
                    if (ext_token == null) {
                        String broker_id = usession.getNote(Details.IDENTITY_PROVIDER);
                        if (broker_id == null) {
                            usession.getNote(IdentityProvider.EXTERNAL_IDENTITY_PROVIDER);
                        }
                        if (broker_id != null && broker_id.equals(provider.getConfig().getAlias())) {
                            ext_token = usession.getNote(FEDERATED_ACCESS_TOKEN);
                        }                        
                    }
                    if (ext_token != null) {
                        try {
                            AccessTokenResponse resp = JsonSerialization.readValue(ext_token, AccessTokenResponse.class);
                            final String refresh_token = resp.getRefreshToken();
                            if (refresh_token != null) {
                                try {
                                    final JWSInput jws = new JWSInput(refresh_token);
                                    final JsonWebToken jwt = jws.readJsonContent(JsonWebToken.class);
                                    if (jwt.isActive()) {                                        
                                        try (VaultStringSecret vaultStringSecret = ksession.vault()
                                                .getStringSecret(provider.getConfig().getClientSecret())) {
                                            final SimpleHttp request = SimpleHttp.doPost(provider.getConfig().getTokenUrl(), ksession)
                                                .param(OAUTH2_GRANT_TYPE_REFRESH_TOKEN, refresh_token)
                                                .param(OAUTH2_PARAMETER_GRANT_TYPE, OAUTH2_GRANT_TYPE_REFRESH_TOKEN)
                                                .param(OAUTH2_PARAMETER_CLIENT_ID, provider.getConfig().getClientId())
                                                .param(OAUTH2_PARAMETER_CLIENT_SECRET, vaultStringSecret.get().orElse(provider.getConfig().getClientSecret()))
                                                .param(OAUTH2_PARAMETER_SCOPE, ext_idp_claim) // ignored in KC 16.1.1 !!!
                                                .acceptJson();
                                            resp = request.asJson(AccessTokenResponse.class);
                                        }
                                        return resp.getToken();
                                    }
                                } catch (Exception ex) {
                                    Logger.getLogger(ExternalIdpScopesProtocolMapper.class.getName()).log(Level.INFO, "invalid refresh token", ex);
                                }
                            }
                            return null;
                        } catch (IOException ex) {
                            Logger.getLogger(ExternalIdpScopesProtocolMapper.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
        return null;
    }
    
    private UserInfo getExternalUserInfo(String endpoint, String token) {
        try {
            URI uri = URI.create(endpoint);
            loop:
            for (int i = 0; i < 10; i++) {
                final URL url = uri.toURL();
                HttpURLConnection con = null;
                try {
                    con = (HttpURLConnection)url.openConnection();

                    con.setReadTimeout(120000);
                    con.setConnectTimeout(300000);
                    con.setInstanceFollowRedirects(false);

                    con.addRequestProperty("Authorization", "Bearer " + token);
                    
                    try (BufferedInputStream in = new BufferedInputStream(con.getInputStream())) {
                        final int code = con.getResponseCode();
                        switch(code) {
                            case HttpURLConnection.HTTP_OK:          
                            case HttpURLConnection.HTTP_NOT_MODIFIED:
                                final String text = new String(in.readAllBytes(), StandardCharsets.UTF_8);
                                return JsonSerialization.mapper.readValue(text, UserInfo.class);                                
                            case HttpURLConnection.HTTP_MOVED_PERM:
                            case HttpURLConnection.HTTP_MOVED_TEMP:
                            case 307: // Temporary Redirect
                            case 308: // Permanent Redirect
                            case HttpURLConnection.HTTP_SEE_OTHER:
                                final String location = con.getHeaderField("Location");
                                if (location == null || location.isEmpty()) {
                                    break loop;
                                }
                                final URI redirect = URI.create(location);
                                uri = redirect.isAbsolute() ? redirect : uri.resolve(redirect);
                                break;
                            default:
                                Logger.getLogger(ExternalIdpScopesProtocolMapper.class.getName()).log(Level.WARNING, "error from external server: {0}", code);
                                break loop;
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ExternalIdpScopesProtocolMapper.class.getName()).log(Level.WARNING, "error reading permissions", ex);
                    if (con != null) {
                        try (InputStream in = con.getErrorStream()) {}
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ExternalIdpScopesProtocolMapper.class.getName()).log(Level.WARNING, "error reading permissions", ex);
        }
        return null;
    }
    
    /**
     * Merge value into the target_claim claim of the token.
     * 
     * 
     * @param token the token to insert an external claim
     * @param model
     * @param ext_claims the map of external token claims
     */
    private void mergeClaim(AccessToken token, ProtocolMapperModel model, Map<String, Object> ext_claims) {
        if (ext_claims != null) {
            final String ext_idp_claim = model.getConfig().get(EXTERNAL_IDP_CLAIM_PROPERTY_NAME);
            final String target_claim = model.getConfig().get(TOKEN_CLAIM_PROPERTY_NAME);
            final Object value = ext_claims.get(ext_idp_claim);
            if (value != null && ext_idp_claim != null && target_claim != null) {
                final Map<String, Object> claims = token.getOtherClaims();
                if (claims == null) {
                    token.setOtherClaims(target_claim, value);
                } else {
                    final Object obj = claims.get(target_claim);
                    if (obj == null) {
                        claims.put(target_claim, value);
                    } else if (value instanceof List) {
                        final List list = (List)value;
                        if (obj instanceof List) {
                            ((List)obj).addAll(list);
                        } else {
                            list.add(obj);
                            claims.put(target_claim, list);
                        }
                    } else if (obj instanceof List) {
                        ((List)obj).add(value);
                    } else {
                        claims.put(target_claim, Arrays.asList(obj, value));
                    }
                }
            }
        }
    }
}
